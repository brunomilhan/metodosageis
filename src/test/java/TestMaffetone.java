import br.utfpr.metodosAgeis.Maffetone;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ppgca on 05/10/15.
 */
public class TestMaffetone {


    @Test
    public void validarIdade() {
        Maffetone maffetone = new Maffetone();

        assertTrue(maffetone.validaIdade(10));

    }

    @Test
    public void calculaBatimentos() {
        Maffetone maffetone = new Maffetone();


        assertEquals(140, maffetone.calculaBatimentos(40));

    }

    @Test
    public void validaProblema(){
        Maffetone maffetone = new Maffetone();

        assertEquals(maffetone.validaProblema("blabla"), 0);

    }

    @Test
    public void testeBatimentosFinal(){
        Maffetone maffetone = new Maffetone();

        assertEquals(130, maffetone.calculaBatimentosFinal(40, "coracao"));
    }

}
