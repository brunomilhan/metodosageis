package br.utfpr.metodosAgeis;

/**
 * Created by ppgca on 05/10/15.
 */
public class Maffetone {

    public boolean validaIdade(int idade) {
        if (idade <= 100 || idade >= 10) {
            return true;
        }
        return false;
    }

    public int calculaBatimentos(int idade) {

        return (180 - idade);
    }

    public int calculaBatimentosFinal(int idade, String problema) {
        int tipoProblema = validaProblema(problema);
            return calculaBatimentos(idade) + tipoProblema;

    }

    public int validaProblema(String problema) {
        if (problema.equals("coracao")) {
            return -10;
        } else if (problema.equals("lesionado")) {
            return -5;
        } else if (problema.equals("desporto")) {
            return 0;
        } else if (problema.equals("profissional")) {
            return 5;
        }
        return 0;
    }
}
